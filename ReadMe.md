File download manager written in javascript to handle multipart file downloads 

development requirements
node version: node 8.9.4 
npm:	5.6.0
platform: windows 10 
electron	1.7.0
electron packager


some usefull reads/links
1. https://electronjs.org/docs
2, https://github.com/electron/electron (electron npm package setup and usage)
3. https://github.com/electron/electron/releases (electron releases)

We can fork and use this module https://www.npmjs.com/package/mt-files-downloader 

we can also look into this https://www.npmjs.com/package/multi-download as this can be injected into the page in chrome using the extension.